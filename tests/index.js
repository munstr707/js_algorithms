const sortingTests = require("./sortingTests");
const searchTests = require("./searchTests");
const arrayTests = require("./arrayTests");
const specificNumbersTests = require("./specificNumbers.tests");

module.exports = {
  sortingTests,
  searchTests,
  arrayTests,
  specificNumbersTests
};
