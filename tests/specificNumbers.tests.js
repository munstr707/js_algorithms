const numberAlgorithms = require("../numbers");
const loggingUtils = require("../testUtils/loggingUtils");

const isIntTests = () => {
  loggingUtils.printTestNameSectionHeader("Is Integer Tests");
  const integer = 1;
  const double = 123.45;

  console.log(
    `is ${double} an integer: ${numberAlgorithms.specificNumberType.isInteger(
      double
    )}`
  );

  console.log(
    `is ${integer} an integer: ${numberAlgorithms.specificNumberType.isInteger(
      integer
    )}`
  );
};

module.exports = {
  isIntTests
};
