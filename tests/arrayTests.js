const arrayAlgorithms = require("../arrays");
const loggingUtils = require("../testUtils/loggingUtils");

const arraySumTests = () => {
  loggingUtils.printTestNameSectionHeader("Array Sum Tests");
  const testArr1 = [1, 2, [3], 4];
  const testArr2 = [1, [11, 42, [8, 1], 4, [22, 21]]];
  const testArr3 = [1, [[[[[[[[[1]]]]]]]], [[[[[[[[1]]]]]]]], [[[[[[[[1]]]]]]]], [[[[[[[[1]]]]]]]]]];

  console.log(arrayAlgorithms.nestedArraySum(testArr1) === 1 + 2 + 3 + 4);
  console.log(arrayAlgorithms.nestedArraySum(testArr2) === 1 + 11 + 42 + 8 + 1 + 4 + 22 + 21);
  console.log(arrayAlgorithms.nestedArraySum(testArr3) === 1 + 1 + 1 + 1 + 1);
};

module.exports = {
  arraySumTests
};
