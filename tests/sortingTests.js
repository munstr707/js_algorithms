const sortingAlgoritms = require("../sorting/");
const loggingUtils = require("../testUtils/loggingUtils");

// bubble sort tests
const bubbleSortTests = () => {
  loggingUtils.printTestNameSectionHeader("Bubble Sort Tests");
  let bubbleSortArray = [5, 4, 3, 2, 1];
  console.log(`bubble sort before: ${bubbleSortArray}`);
  sortingAlgoritms.bubbleSort(bubbleSortArray);
  console.log(`bubble sort after: ${bubbleSortArray}`)
}

module.exports = {
  bubbleSortTests
}
