const searchAlgorithms = require("../search/");
const objFactories = require("../testFactories/objArrays");
const loggingUtils = require("../testUtils/loggingUtils");

const binarySearchTests = () => {
  loggingUtils.printTestNameSectionHeader("Binary Search Tests");
  const dataSet = objFactories.animalArr("cat", "dog", "bird", "fish", "bug", "whale", "monkey");

  console.log(searchAlgorithms.binarySearch(dataSet, -1));
  console.log(searchAlgorithms.binarySearch(dataSet, 1));
  console.log(searchAlgorithms.binarySearch(dataSet, 2));
  console.log(searchAlgorithms.binarySearch(dataSet, 3));
  console.log(searchAlgorithms.binarySearch(dataSet, 4));
  console.log(searchAlgorithms.binarySearch(dataSet, 5));
  console.log(searchAlgorithms.binarySearch(dataSet, 6));
  console.log(searchAlgorithms.binarySearch(dataSet, 7));
  console.log(searchAlgorithms.binarySearch(dataSet, 8));
};

const findBadVerstionTests = () => {
  loggingUtils.printTestNameSectionHeader("Find Bad Version tests");
  searchAlgorithms.findBadVersion.setIsBadNum(123);
  const badVersion = searchAlgorithms.findBadVersion.findBadVersion(1000, 1);
  console.log(`the bad version is: ${badVersion}`);
};

module.exports = {
  binarySearchTests,
  findBadVerstionTests
};
