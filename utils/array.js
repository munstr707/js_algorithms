const printArr = (arr = [0]) => arr.map(element => console.log(element));

module.exports = {
  printArr
}