const bubbleSort = (arr = [0]) => {
  for (let i = 0, length = arr.length; i < length - 1; i++) {
    for (let j = 0; j < length - 1 - i; j++) {
      let currElement = arr[j];
      let nextElement = arr[j + 1];
      if (currElement > nextElement) {
        arr[j] = nextElement;
        arr[j + 1] = currElement
      }
    }
  }
}

module.exports = {
  bubbleSort
}
