const bubbleSort = require("./bubbleSort");

module.exports = {
  bubbleSort: bubbleSort.bubbleSort
};