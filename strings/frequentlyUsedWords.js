function mostUsedWords(literatureText = "", wordsToExclude = [""]) {
  return removeWordsFromString(literatureText, wordsToExclude);
}

function removeWordsFromString(string = "", wordsToExcludeList = [""]) {
  const regExp = new RegExp(wordsToExcludeList.join("|"), "g");
  return string.replace(regExp, " ");
}

const exampleText =
  "Jack and Jill went up to the market to buy bread and cheese. Cheese is Jack's and Jill's favorite food.";

const exampleWordsToExclude = ["and", "he", "the", "to", "is", "Jack", "Jill"];

console.log(mostUsedWords(exampleText, exampleWordsToExclude));
