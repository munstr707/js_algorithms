const printTestNameSectionHeader = (testName = "") => console.log(`---------------------- ${testName} -------------------------`);

module.exports = {
  printTestNameSectionHeader
}
