/**
 * Take an array with arbitrary nested arrays in it and return the sum of the numbers within
 * Ex [1, [2]] = 3
 */

const nestedArraySum = (numArray = [0]) => numArray.reduce((sum, currentElement) => {
  return sum + (Array.isArray(currentElement) ? nestedArraySum(currentElement) : currentElement);
}, 0);

module.exports = nestedArraySum;
