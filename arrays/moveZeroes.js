// two convoluted answers not good for production but nonetheless efficient and "elite"
const moveZeroes = (intArr = [0]) => {
  let lastNonZeroIndex = 0;
  intArr.forEach((int, index) => {
    if (int !== 0) {
      intArr[lastNonZeroIndex++] = intArr[index];
    }
  });
  while (lastNonZeroIndex < intArr.length) {
    intArr[lastNonZeroIndex++] = 0;
  }
};

let testArr = [0, 1, 0, 3, 12];
moveZeroes(testArr);
console.log(testArr);

const moveZeroesSuccinct = (intArr = [0]) => {
  let lastNonZeroIndex = 0;
  intArr.forEach((int, index) => {
    if (int !== 0) {
      intArr[index] = intArr[lastNonZeroIndex];
      intArr[lastNonZeroIndex++] = int;
    }
  });
};

let testArr2 = [0, 1, 0, 3, 12];
moveZeroesSuccinct(testArr2);
console.log(testArr2);
