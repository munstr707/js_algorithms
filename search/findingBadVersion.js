/**
 * You are given a number representing a bad version.
 * Each bad version after the first bad version is bad.
 * Givin a function which can return whether or not a version is bad, find the bad version.
 *
 * assumptions: versions start at 1
 */

const findBadVersion = (max, min) => {
  const pivot = Math.floor((max + min) / 2);
  if (pivot === min) {
    return isBad(pivot) ? pivot : max;
  } else if (isBad(pivot)) {
    return findBadVersion(pivot, min);
  } else {
    return findBadVersion(max, pivot);
  }
};

const isBad = (num) => num >= isBadNum;

let isBadNum = 3;

function setIsBadNum(num) {
  isBadNum = num;
}

module.exports = { findBadVersion, setIsBadNum };
