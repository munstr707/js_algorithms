const binarySearch = require("./binarySearch");
const findBadVersion = require("./findingBadVersion");

module.exports = {
  binarySearch: binarySearch.binarySearch,
  findBadVersion
};
