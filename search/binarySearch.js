/**
 * Optimized this algorithm for simplicity over some traditional solutions:
 *  a. it only takes 2 args
 *  b. the math portion for calculating the pivot is intentionally simpler
 */
const binarySearch = (arr = [], targetId = 0) => {
  const pivot = Math.floor(arr.length / 2);
  if (arr[pivot].id === targetId) {
    return arr[pivot];
  } else if (arr.length === 1) {
    return -1;
  } else if (targetId < arr[pivot].id) {
    return binarySearch(arr.slice(0, pivot), targetId);
  } else if (targetId > arr[pivot].id) {
    return binarySearch(arr.slice(pivot), targetId);
  }
};

module.exports = {
  binarySearch
};
