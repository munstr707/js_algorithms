const isInteger = (num) => num % 1 === 0;

module.exports = {
  isInteger
};
