const animalArr = (...animalNames) => animalNames.map((name, index) => ({
  id: index + 1,
  name: name,
}));

module.exports = {
  animalArr
};